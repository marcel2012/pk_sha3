FROM fedora AS builder

RUN yum install -y mpich-devel redhat-rpm-config

COPY main.c src/app.c

RUN /usr/lib64/mpich/bin/mpicc src/app.c -o app

FROM builder as final

ENTRYPOINT time /usr/lib64/mpich/bin/mpiexec -n 30 ./app
